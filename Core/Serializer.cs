﻿using System.Text;
using Newtonsoft.Json;

namespace Tolltech.Core
{
    public class Serializer : ISerializer
    {
        public byte[] Serialze<T>(T obj)
        {
            return Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(obj));
        }

        public string SerialzeStr<T>(T obj)
        {
            return JsonConvert.SerializeObject(obj);
        }

        public T Deserialze<T>(string str)
        {
            return JsonConvert.DeserializeObject<T>(str);
        }

        public T Deserialze<T>(byte[] bytes)
        {
            return JsonConvert.DeserializeObject<T>(Encoding.UTF8.GetString(bytes));
        }
    }
}