﻿namespace Tolltech.Core
{
    public interface ISerializer
    {
        byte[] Serialze<T>(T obj);
        string SerialzeStr<T>(T obj);
        T Deserialze<T>(string str);
        T Deserialze<T>(byte[] bytes);
    }
}