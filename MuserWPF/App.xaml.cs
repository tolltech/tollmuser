﻿using System.Windows;
using System.Windows.Threading;
using log4net;
using Ninject;

namespace MuserWPF
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private IKernel container;

        private static readonly ILog log = LogManager.GetLogger(typeof(App));

        protected override void OnStartup(StartupEventArgs e)
        {
            log.Info($"Start Muser app");

            base.OnStartup(e);

            log.Info($"Configuring DI container");

            ConfigureContainer();

            log.Info($"DI container is ready");

            DispatcherUnhandledException += App_DispatcherUnhandledException;

            Current.MainWindow = this.container.Get<MainWindow>();
            Current.MainWindow.Show();
        }

        private void ConfigureContainer()
        {
            this.container = new StandardKernel(new ConfigurationModule("lognet.config"));
        }

        private void App_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            log.Info($"UNHANDLED EXCEPTION {e.Exception.Message} {e.Exception.StackTrace}");
            MessageBox.Show($"Неизвестная ошибка.\r\n{e.Exception.Message}\r\n{e.Exception.StackTrace}");
            e.Handled = true;
        }
    }
}
