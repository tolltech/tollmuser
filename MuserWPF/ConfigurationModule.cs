﻿using System.IO;
using log4net.Config;
using Ninject.Modules;
using Tolltech.Core;

namespace MuserWPF
{
    public class ConfigurationModule : NinjectModule
    {
        private readonly string log4NetConfigFilename;

        public ConfigurationModule(string log4netConfigFilename = null)
        {
            log4NetConfigFilename = log4netConfigFilename;
        }

        public override void Load()
        {
            if (!string.IsNullOrWhiteSpace(log4NetConfigFilename))
            {
                var fileInfo = new FileInfo(log4NetConfigFilename);
                if (fileInfo.Exists)
                {
                    XmlConfigurator.Configure(fileInfo);
                }
            }

            IoCResolver.Resolve((@interface, implementation) => this.Bind(@interface).To(implementation), "Tolltech", "MuserWPF", "Muser");
        }
    }
}