﻿using System;
using Microsoft.Win32;
using Tolltech.Core;

namespace MuserWPF.Settings
{
    public class AuthorizationSettings : IAuthorizationSettings
    {
        private readonly ISerializer serializer;
        private readonly ICryptoService cryptoService;

        private static MuserAuthorization authorization;

        private const string regNameKey = @"SOFTWARE\TollMuser";
        private const string regNameSubKey = "ServiceInfo";

        public AuthorizationSettings(ISerializer serializer, ICryptoService cryptoService)
        {
            this.serializer = serializer;
            this.cryptoService = cryptoService;
        }

        public bool HasStoredAuthorizartion
        {
            get
            {
                var key = Registry.CurrentUser.OpenSubKey(regNameKey);
                var value = key?.GetValue(regNameSubKey)?.ToString();
                key?.Close();
                return !string.IsNullOrEmpty(value);
            }
        }

        public MuserAuthorization GetAndCacheMuserAuthorization(string cryptoKey)
        {
            if (authorization != null)
            {
                return authorization;
            }

            var key = Registry.CurrentUser.OpenSubKey(regNameKey);
            var value = key?.GetValue(regNameSubKey)?.ToString();
            key?.Close();

            if (value == null)
            {
                return null;
            }

            var serializedAuth = cryptoService.Decrypt(value, cryptoKey);
            try
            {
                return authorization = serializer.Deserialze<MuserAuthorization>(serializedAuth);
            }
            catch (Exception e)
            {
                return null;
            }            
        }

        public void SetMuserAuthorization(string cryptoKey, MuserAuthorization authorization)
        {
            if (!string.IsNullOrEmpty(cryptoKey))
            {
                var key = Registry.CurrentUser.CreateSubKey(regNameKey);

                var seralizedAuth = serializer.SerialzeStr(authorization);
                var crypted = cryptoService.Encrypt(seralizedAuth, cryptoKey);

                key.SetValue(regNameSubKey, crypted);
                key.Close();
            }

            AuthorizationSettings.authorization = authorization;
        }

        public MuserAuthorization MuserAuthorization => authorization;
        public bool Authorized => MuserAuthorization != null;
    }
}