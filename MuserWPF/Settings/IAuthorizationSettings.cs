﻿using JetBrains.Annotations;

namespace MuserWPF.Settings
{
    public interface IAuthorizationSettings
    {
        bool HasStoredAuthorizartion { get; }

        [CanBeNull]
        MuserAuthorization GetAndCacheMuserAuthorization([NotNull] string cryptoKey);
        void SetMuserAuthorization([CanBeNull] string cryptoKey, [NotNull] MuserAuthorization authorization);

        [CanBeNull] MuserAuthorization MuserAuthorization { get; }
        bool Authorized { get; }
    }
}