﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using MuserWPF.Domain;
using MuserWPF.Domain.Models;
using MuserWPF.Settings;
using Tolltech.Core;
using Toltech.YandexApiClient;

namespace MuserWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow(ISerializer serializer, IAuthorizationSettings authorizationSettings, IVkService vkService,
            IYandexService yandexService, IDomainService domainService)
        {
            InitializeComponent();

            this.serializer = serializer;
            this.authorizationSettings = authorizationSettings;
            this.vkService = vkService;
            this.yandexService = yandexService;
            this.domainService = domainService;

            foregroundBrush = vkLoginTextBox.Foreground;

            InitAuthorization();
            SavePopupStates();
        }

        private void InitAuthorization()
        {
            IsEnabled = false;
            startAuthorizePopup.IsOpen = true;
            startCryptoPasswordPasswordBox.Focus();
        }

        private readonly ISerializer serializer;
        private readonly IAuthorizationSettings authorizationSettings;
        private readonly IVkService vkService;
        private readonly IYandexService yandexService;
        private readonly IDomainService domainService;

        private IYandexMusicClient yandexApi;

        private readonly Brush foregroundBrush;

        private async Task RefreshDataAsync()
        {
            if (!authorizationSettings.Authorized)
            {
                infoTextBlock.Foreground = redBrush;
                infoTextBlock.Content = "Пожалуйста, авторизуйтесь!";
                return;
            }

            infoTextBlock.Foreground = foregroundBrush;
            infoTextBlock.Content = string.Empty;

            yandexApi = await yandexService.GetClientAsync().ConfigureAwait(true);

            var yaPlaylistId = await GetYaPlaylistIdForImport().ConfigureAwait(true);
            var syncTracks = await domainService.GetNewVkTracksAsync(yaPlaylistId).ConfigureAwait(true);

            vkListView.Items.Clear();
            foreach (var track in syncTracks)
            {
                vkListView.Items.Add(new ListViewItem
                {
                    Content = $"{track.Artist} - {track.Title}",
                    Tag = serializer.SerialzeStr(track)
                });
            }

            await RefreshYandexTracksAsync(yaPlaylistId).ConfigureAwait(true);
        }

        #region - sync popups -

        private readonly ConcurrentDictionary<Popup, bool> popupOpenStates = new ConcurrentDictionary<Popup, bool>();

        private void SavePopupStates()
        {
            this.Deactivated += OnDeactivvated;
            this.Activated += OnActivvated;
        }

        private void OnDeactivvated(object sender, EventArgs eArgs)
        {
            foreach (var popup in new[] {startAuthorizePopup, progressBarPopup})
            {
                popupOpenStates.AddOrUpdate(popup, startAuthorizePopup.IsOpen, (p, b) => p.IsOpen);
                popup.IsOpen = false;
            }
        }

        private void OnActivvated(object sender, EventArgs eArgs)
        {
            foreach (var popupOpenState in popupOpenStates)
            {
                var wasOpen = popupOpenState.Value;
                var popup = popupOpenState.Key;
                popup.IsOpen = wasOpen;
            }
        }

        #endregion

        private async Task<string> GetYaPlaylistIdForImport()
        {
            if (yaPlayListsComboBox.Items.Count > 0 && yaPlayListsComboBox.SelectedItem != null)
            {
                var comboBoxItem = (ComboBoxItem) yaPlayListsComboBox.SelectedItem;
                return comboBoxItem.Tag.ToString();
            }

            const string title = "Vk";
            var playlists = (await yandexApi.GetPlaylistsAsync().ConfigureAwait(true))
                .OrderBy(x => x.Title == title ? 0 : 1)
                .ThenBy(x => x.Title)
                .ToArray();

            yaPlayListsComboBox.Items.Clear();

            if (playlists.Any())
            {
                foreach (var playlist in playlists)
                {
                    yaPlayListsComboBox.Items.Add(new ComboBoxItem
                    {
                        Content = playlist.Title,
                        Tag = playlist.Id
                    });
                }

                var first = playlists.First();

                if (first.Title == title)
                {
                    return first.Id;
                }
            }

            var vkPlaylist = await yandexApi.CreatePlaylistAsync(title).ConfigureAwait(true);

            yaPlayListsComboBox.Items.Add(new ComboBoxItem
            {
                Content = vkPlaylist.Title,
                Tag = serializer.SerialzeStr(vkPlaylist)
            });

            return vkPlaylist.Id;
        }

        private async void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            await RefreshDataAsync().ConfigureAwait(true);
        }

        private async void ImportAllButton_Click_1(object sender, RoutedEventArgs e)
        {
            var items = vkListView.SelectedItems;
            if (items.Count <= 0)
            {
                items = vkListView.Items;
            }

            var trackToImport = new List<VkTrack>(vkListView.Items.Count);
            foreach (var item in items.Cast<ListViewItem>())
            {
                trackToImport.Add(serializer.Deserialze<VkTrack>(item.Tag.ToString()));
            }

            var playlistId = await GetYaPlaylistIdForImport().ConfigureAwait(true);

            var worker = new BackgroundWorker
            {
                WorkerReportsProgress = true
            };
            worker.ProgressChanged += worker_ProgressChanged;
            worker.DoWork += (o, args) =>
            {
                domainService.ImportTracksAsync(trackToImport.ToArray(), playlistId, x =>
                {
                    var percentProgress = x.Total != 0 ? x.Processed * 1.0m / x.Total * 100 : 100;
                    ((BackgroundWorker) o).ReportProgress((int) percentProgress, $"{x.Processed}/{x.Total}");
                }).GetAwaiter().GetResult();
            };

            progressBar.Value = 0;
            progressBarTextBlock.Content = string.Empty;
            progressBarPopup.IsOpen = true;
            worker.RunWorkerAsync();
        }

        private async void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBarTextBlock.Content = e.UserState.ToString();
            if (e.ProgressPercentage == 100)
            {
                progressBar.Value = e.ProgressPercentage;

                await RefreshDataAsync().ConfigureAwait(true);
                progressBarPopup.IsOpen = false;
                IsEnabled = true;
                return;
            }

            IsEnabled = false;
            progressBar.Value = e.ProgressPercentage;
        }

        private void AuthorizeButton_Click(object sender, RoutedEventArgs e)
        {
            authorizePopup.IsOpen = true;
            vkLoginTextBox.Focus();
        }

        private static readonly Brush redBrush = new SolidColorBrush(Colors.Red);
        private static readonly Brush backGroundTextBoxBrush = new SolidColorBrush(new Color {B = 24, G = 24, R = 24});

        private async void OkAuthorizePopupButon_Click(object sender, RoutedEventArgs e)
        {
            var vkLogin = vkLoginTextBox.Text;
            var vkPassword = vkPasswordPasswordBox.Password;

            if (!await vkService.CheckCredentialsAsync(vkLogin, vkPassword).ConfigureAwait(true))
            {
                vkPasswordPasswordBox.Background = redBrush;
                return;
            }
            else
            {
                vkPasswordPasswordBox.Background = backGroundTextBoxBrush;
            }

            var yaLogin = yaLoginTextBox.Text;
            var yaPassword = yaPasswordPasswordBox.Password;

            if (!await yandexService.CheckCredentialsAsync(yaLogin, yaPassword).ConfigureAwait(true))
            {
                yaPasswordPasswordBox.Background = redBrush;
                return;
            }
            else
            {
                yaPasswordPasswordBox.Background = backGroundTextBoxBrush;
            }

            var crypto = cryptoKeyPasswordPasswordBox.Password;
            var storePassword = (rememberAuthorizeCheckbox.IsChecked ?? false) && !string.IsNullOrEmpty(crypto);
            var muserAuthorization = new MuserAuthorization
            {
                VkLogin = vkLogin,
                VkPassword = vkPassword,
                YaLogin = yaLogin,
                YaPassword = yaPassword
            };
            authorizationSettings.SetMuserAuthorization(storePassword ? crypto : null, muserAuthorization);

            authorizePopup.IsOpen = false;
            await RefreshDataAsync().ConfigureAwait(true);
        }

        private void CancelAuthorizePopupButon_Click(object sender, RoutedEventArgs e)
        {
            authorizePopup.IsOpen = false;
        }

        private void CryptiKeyPasswordPasswordBox_OnPasswordChanged(object sender, RoutedEventArgs e)
        {
            var pass = cryptoKeyPasswordPasswordBox.Password;
            rememberAuthorizeCheckbox.IsEnabled = !string.IsNullOrEmpty(pass);
        }

        private async void StartCancelAuthorizePopupButon_OnClick(object sender, RoutedEventArgs e)
        {
            startAuthorizePopup.IsOpen = false;
            await RefreshDataAsync().ConfigureAwait(true);
            IsEnabled = true;
        }

        private async void StartOkAuthorizePopupButon_OnClick(object sender, RoutedEventArgs e)
        {
            await StartAuthorizePopupComplete().ConfigureAwait(true);
        }

        private async Task StartAuthorizePopupComplete()
        {
            var password = startCryptoPasswordPasswordBox.Password;
            if (!string.IsNullOrEmpty(password))
            {
                var authorization = authorizationSettings.GetAndCacheMuserAuthorization(password);
                if (authorization == null && authorizationSettings.HasStoredAuthorizartion)
                {
                    MessageBox.Show("Неверный пароль! Попробуйте ввести другой, или продолжите без пароля.");
                    startCryptoPasswordPasswordBox.Clear();
                    return;
                }

                cryptoKeyPasswordPasswordBox.Password = password;
            }

            startAuthorizePopup.IsOpen = false;
            IsEnabled = true;
            await RefreshDataAsync().ConfigureAwait(true);
        }

        private async void YaPlayListsComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            await RefreshDataAsync().ConfigureAwait(true);
        }

        private async Task RefreshYandexTracksAsync(string playListId)
        {
            var tracks = await yandexApi.GetTracksAsync(playListId).ConfigureAwait(true);

            yaListView.Items.Clear();

            foreach (var track in tracks)
            {
                yaListView.Items.Add($"{string.Join(", ", track.Artists.Select(x => x.Name))} - {track.Title}");
            }
        }

        private async void StartCryptoPasswordPasswordBox_OnKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                await StartAuthorizePopupComplete().ConfigureAwait(true);
            }
        }
    }
}