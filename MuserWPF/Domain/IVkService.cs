﻿using System.Threading.Tasks;
using MuserWPF.Domain.Models;

namespace MuserWPF.Domain
{
    public interface IVkService
    {
        Task<bool> CheckCredentialsAsync(string login, string password);
        Task<VkTrack[]> GetVkTracksAsync();
    }
}