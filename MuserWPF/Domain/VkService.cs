﻿using System.Collections.Concurrent;
using System.Linq;
using System.Threading.Tasks;
using JetBrains.Annotations;
using MuserWPF.Domain.Models;
using MuserWPF.Settings;
using Tolltech.VkApiClient;

namespace MuserWPF.Domain
{
    public class VkService : IVkService
    {
        private readonly IAuthorizationSettings authorizationSettings;

        private static readonly ConcurrentDictionary<(string, string), IVkApiClient> vkClients =
            new ConcurrentDictionary<(string, string), IVkApiClient>();

        public VkService(IAuthorizationSettings authorizationSettings)
        {
            this.authorizationSettings = authorizationSettings;
        }

        public async Task<bool> CheckCredentialsAsync(string login, string password)
        {
            try
            {
                await GetClientAsync(login, password).ConfigureAwait(false);
            }
            catch (VkAuthorizeException)
            {
                return false;
            }

            return true;
        }

        //todo: srp?
        public async Task<VkTrack[]> GetVkTracksAsync()
        {
            var client = await GetClientAsync().ConfigureAwait(false);
            return (await client.GetTracksAsync().ConfigureAwait(false))
                .Select(x => new VkTrack
                {
                    Title = x.Title,
                    Artist = x.Artist
                })
                .ToArray();
        }

        [ItemNotNull]
        private async Task<IVkApiClient> GetClientAsync(string login = null, string password = null)
        {
            login = login ?? authorizationSettings.MuserAuthorization?.VkLogin;
            password = password ?? authorizationSettings.MuserAuthorization?.VkPassword;

            if (string.IsNullOrWhiteSpace(login) || string.IsNullOrEmpty(password))
            {
                throw new VkAuthorizeException();
            }

            var client = vkClients.GetOrAdd((login, password), tuple => new VkApiClient(login, password));
            if (!await client.AuthorizeAsync().ConfigureAwait(false))
            {
                throw new VkAuthorizeException();
            }

            return client;
        }
    }
}