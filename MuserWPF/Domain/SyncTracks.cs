﻿using System.Collections.Generic;
using System.Linq;
using log4net;
using MuserWPF.Domain.Models;

namespace MuserWPF.Domain
{
    public class SyncTracks
    {
        private readonly VkTrack[] vkTracks;
        private readonly HashSet<(string Artist, string Title)> yaTracksHashSet;

        private static readonly ILog log = LogManager.GetLogger(typeof(SyncTracks));

        public SyncTracks(VkTrack[] vkTracks, Toltech.YandexApiClient.ApiModels.Track[] yaTracks)
        {
            this.vkTracks = vkTracks;
            var trackTuples = yaTracks.SelectMany(x => x.Artists.Select(y => (Artist: y.Name, Title: x.Title)));
            yaTracksHashSet = new HashSet<(string Artist, string Title)>(trackTuples);
        }

        public VkTrack[] GetNewTracks()
        {
            var newTracks = vkTracks
                .Where(x => x.Artists.All(y => !yaTracksHashSet.Contains((Artist: y, Title: x.Title))))
                .ToArray();

            log.Info($"Found {newTracks.Length} new tracks");

            return newTracks;
        }
    }
}