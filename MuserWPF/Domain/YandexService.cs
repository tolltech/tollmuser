﻿using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using JetBrains.Annotations;
using MuserWPF.Settings;
using Tolltech.Core;
using Toltech.YandexApiClient;
using Toltech.YandexApiClient.Authorizations;

namespace MuserWPF.Domain
{
    public class YandexService : IYandexService
    {
        private readonly IAuthorizationSettings authorizationSettings;
        private readonly ISerializer serializer;

        private static readonly ConcurrentDictionary<(string, string), Task<IYandexMusicClient>> yaClients =
            new ConcurrentDictionary<(string, string), Task<IYandexMusicClient>>();

        public YandexService(IAuthorizationSettings authorizationSettings, ISerializer serializer)
        {
            this.authorizationSettings = authorizationSettings;
            this.serializer = serializer;
        }

        public async Task<bool> CheckCredentialsAsync(string login, string password)
        {
            try
            {
                await InnerGetClientAsync(login, password).ConfigureAwait(false);
            }
            catch (YaAuthorizeException e)
            {
                return false;
            }

            return true;
        }

        public Task<IYandexMusicClient> GetClientAsync()
        {
            return InnerGetClientAsync();
        }

        [ItemNotNull]
        private async Task<IYandexMusicClient> InnerGetClientAsync(string login = null, string password = null)
        {
            login = login ?? authorizationSettings.MuserAuthorization?.YaLogin;
            password = password ?? authorizationSettings.MuserAuthorization?.YaPassword;

            if (string.IsNullOrWhiteSpace(login) || string.IsNullOrEmpty(password))
            {
                throw new YaAuthorizeException();
            }

            var client =
                await yaClients.GetOrAdd((login, password), tuple => CreateClientAsync(tuple.Item1, tuple.Item2));

            if (client == null)
            {
                throw new VkAuthorizeException();
            }

            return client;
        }

        [ItemCanBeNull]
        private async Task<IYandexMusicClient> CreateClientAsync(string login, string password)
        {
            var yandexCredentials = new YandexCredentials(serializer, login, password);
            try
            {
                var authorizationInfo = await yandexCredentials.GetAuthorizationInfoAsync().ConfigureAwait(false);
            }
            catch (Exception e)
            {
                return null;
            }

            return new YandexMusicClient(yandexCredentials, serializer);
        }
    }
}