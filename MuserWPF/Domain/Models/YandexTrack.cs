﻿namespace MuserWPF.Domain.Models
{
    public class YandexTrack
    {
        public string Artist { get; set; }
        public string Title { get; set; }
        public string Id { get; set; }
        public string AlbumId { get; set; }
    }
}