﻿using System;

namespace MuserWPF.Domain.Models
{
    public class VkTrack
    {
        public string Artist { get; set; }
        public string Title { get; set; }

        public string[] Artists => Artist.Split(new[] {","}, StringSplitOptions.RemoveEmptyEntries);
    }
}