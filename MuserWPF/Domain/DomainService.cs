﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using log4net;
using MuserWPF.Domain.Models;
using Toltech.YandexApiClient;
using Toltech.YandexApiClient.ApiModels;

namespace MuserWPF.Domain
{
    public class DomainService : IDomainService
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(DomainService));

        private readonly IYandexService yandexService;
        private readonly IVkService vkService;

        public DomainService(IYandexService yandexService, IVkService vkService)
        {
            this.yandexService = yandexService;
            this.vkService = vkService;
        }

        public async Task<VkTrack[]> GetNewVkTracksAsync(string yaPlaylistId)
        {
            var yandexApi = await yandexService.GetClientAsync().ConfigureAwait(false);
            var vkTracks = await vkService.GetVkTracksAsync().ConfigureAwait(false);
            var yaTracks = await yandexApi.GetTracksAsync(yaPlaylistId).ConfigureAwait(false);

            log.Info($"Found {yaTracks.Length} yaTracks from playlist {yaPlaylistId} and {vkTracks.Length} vkTracks");

            return new SyncTracks(vkTracks, yaTracks).GetNewTracks();
        }

        public async Task ImportTracksAsync(VkTrack[] trackToImport, string playlistId, Action<(int Processed, int Total)> percentsComplete = null)
        {
            var yandexApi = await yandexService.GetClientAsync().ConfigureAwait(false);

            var foundTracks = new HashSet<(string Id, string AlbumId)>();
            var completeCount = 0;
            var totalCount = trackToImport.Length;

            log.Info($"Start import {totalCount} tracks");

            foreach (var track in trackToImport)
            {
                try
                {
                    log.Info($"START process {track.Artist} - {track.Title} ({completeCount}/{totalCount})");

                    var yaTrack = (await yandexApi.SearchAsync($"{track.Title} {track.Artist}").ConfigureAwait(false))
                        .FirstOrDefault();
                    if (yaTrack == null
                        || yaTrack.Title != track.Title
                        || yaTrack.Artists.All(x => !track.Artists.Contains(x.Name))
                        || yaTrack.Albums.Length <= 0)
                    {
                        log.Info($"SKIP Not found {track.Artist} - {track.Title} ({completeCount}/{totalCount})");
                        continue;
                    }

                    var trackHash = (Id: yaTrack.Id, AlbumId: yaTrack.Albums.First().Id);
                    if (foundTracks.Contains(trackHash))
                    {
                        continue;
                    }
                    else
                    {
                        foundTracks.Add(trackHash);
                    }

                    var playlists = await yandexApi.GetPlaylistsAsync().ConfigureAwait(false);
                    var revision = playlists.FirstOrDefault(x => x.Id == playlistId)?.Revision;

                    var trackToChange = new TrackToChange {Id = trackHash.Id, AlbumId = trackHash.AlbumId};
                    await yandexApi.AddTracksToPlaylistAsync(playlistId, revision, trackToChange).ConfigureAwait(false);

                    await Task.Delay(200).ConfigureAwait(false);
                }
                catch (YandexApiException ex)
                {
                    log.Info($"ERROR YandexApiError {ex.Message}");
                }
                finally
                {
                    percentsComplete?.Invoke((++completeCount, totalCount));
                    log.Info($"PROCESSED {completeCount}/{totalCount} tracks");
                }                
            }
        }
    }
}