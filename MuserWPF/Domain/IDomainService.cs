﻿using System;
using System.Threading.Tasks;
using MuserWPF.Domain.Models;

namespace MuserWPF.Domain
{
    public interface IDomainService
    {
        Task<VkTrack[]> GetNewVkTracksAsync(string yaPlaylistId);
        Task ImportTracksAsync(VkTrack[] trackToImport, string playlistId, Action<(int Processed, int Total)> percentsComplete = null);
    }
}