﻿using System.Threading.Tasks;
using Toltech.YandexApiClient;

namespace MuserWPF.Domain
{
    public interface IYandexService
    {
        Task<bool> CheckCredentialsAsync(string login, string password);
        Task<IYandexMusicClient> GetClientAsync();
    }
}