﻿using System;

namespace Toltech.YandexApiClient
{
    public class YandexApiException : Exception
    {
        public YandexApiException(string msg) : base(msg)
        {
        }
    }
}