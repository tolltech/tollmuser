﻿using System;

namespace Toltech.YandexApiClient.Authorizations
{
    public class AuthorizationInfo
    {
        public string Token { get; set; }
        public string Uid { get; set; }
        public DateTime ExpirationDate { get; set; }
    }
}