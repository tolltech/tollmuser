﻿using Newtonsoft.Json;

namespace Toltech.YandexApiClient.Authorizations
{
    public class FirstStepResponse
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
    }
}