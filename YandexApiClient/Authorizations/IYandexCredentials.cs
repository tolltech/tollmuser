﻿using System.Threading.Tasks;

namespace Toltech.YandexApiClient.Authorizations
{
    public interface IYandexCredentials
    {
        Task<AuthorizationInfo> GetAuthorizationInfoAsync();
        string GetAuthorizeUrl();
    }
}