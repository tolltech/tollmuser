﻿using Newtonsoft.Json;

namespace Toltech.YandexApiClient.ApiModels
{
    public class Album
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }
    }
}