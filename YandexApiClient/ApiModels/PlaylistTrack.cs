﻿using Newtonsoft.Json;

namespace Toltech.YandexApiClient.ApiModels
{
    public class PlaylistTrack
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("track")]
        public Track Track { get; set; }
    }
}