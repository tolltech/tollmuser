﻿using Newtonsoft.Json;

namespace Toltech.YandexApiClient.ApiModels
{
    public class TrackSearchApiResult
    {
        [JsonProperty("tracks")]
        public TrackSearchResult Tracks { get; set; }
    }
}