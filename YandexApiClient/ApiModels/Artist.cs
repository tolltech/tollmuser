﻿using Newtonsoft.Json;

namespace Toltech.YandexApiClient.ApiModels
{
    public class Artist
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}