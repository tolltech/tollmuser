﻿using Newtonsoft.Json;

namespace Toltech.YandexApiClient.ApiModels
{
    public class TrackToChange
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("albumId")]
        public string AlbumId { get; set; }
    }
}