﻿using Newtonsoft.Json;

namespace Toltech.YandexApiClient.ApiModels
{
    public class ApiResult<T>
    {
        [JsonProperty("result")]
        public T Resilt { get; set; }

        [JsonProperty("error")]
        public ApiError Error { get; set; }
    }
}
