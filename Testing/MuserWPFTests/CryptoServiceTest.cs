﻿using System;
using MuserWPF.Settings;
using Ninject;
using NUnit.Framework;

namespace Tolltech.Testing.MuserWPFTests
{
    public class CryptoServiceTest : TestBase
    {
        private ICryptoService cryptoService;

        protected override void SetUp()
        {
            base.SetUp();

            cryptoService = container.Get<ICryptoService>();
        }

        [TestCase("")]
        [TestCase("   ")]
        [TestCase("123")]
        [TestCase("Пароль")]
        [TestCase("!superPas$")]
        [TestCase("@")]
        public void TestCrypt(string src)
        {
            var key1 = Guid.NewGuid().ToString();
            var key2 = Guid.NewGuid().ToString();
            var encrypted = cryptoService.Encrypt(src, key1);
            Assert.AreNotEqual(encrypted, src);

            var decrypted = cryptoService.Decrypt(encrypted, key1);
            Assert.AreEqual(src, decrypted);

            var encryptOther = cryptoService.Encrypt(src, key2);
            Assert.AreNotEqual(encrypted, encryptOther);

            var decryptOther = cryptoService.Decrypt(encrypted, key2);
            Assert.IsNull(decryptOther);
        }
    }
}