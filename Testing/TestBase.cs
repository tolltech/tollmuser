﻿using MuserWPF.Settings;
using Ninject;
using NUnit.Framework;
using Tolltech.Testing.Common;

namespace Tolltech.Testing
{
    [TestFixture]
    public abstract class TestBase
    {
        protected StandardKernel container;
        private CryptoService s;

        [SetUp]
        protected virtual void SetUp()
        {
            s = new CryptoService();//note: for ninject
            container = new StandardKernel(new ConfigurationModule());
        }
    }
}