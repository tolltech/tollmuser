﻿using System.Linq;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Tolltech.Testing
{
    public class VkApiClientTest : TestBase
    {
        private VkApiClient.VkApiClient vkApiClient;

        protected override void SetUp()
        {
            base.SetUp();

            vkApiClient = new VkApiClient.VkApiClient("alexandrovpe2@yandex.ru", "tc_123456");
        }

        [Test]
        public async Task TestGetAudios()
        {
            var actuals = await vkApiClient.GetTracksAsync().ConfigureAwait(false);
            Assert.IsNotEmpty(actuals);
            Assert.IsNotEmpty(actuals.First().Title);
            Assert.IsNotEmpty(actuals.First().Artist);

            var actuals2 = await vkApiClient.GetTracksAsync(2).ConfigureAwait(false);
            Assert.AreEqual(actuals.Length, actuals2.Length);

        }
    }
}