﻿using Ninject.Modules;
using Tolltech.Core;

namespace Tolltech.Testing.Common
{
    public class ConfigurationModule : NinjectModule
    {
        public override void Load()
        {
            IoCResolver.Resolve((@interface, implementation) => this.Bind(@interface).To(implementation), "Tolltech", "MuserWPF");
        }
    }
}