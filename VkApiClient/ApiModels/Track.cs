﻿namespace Tolltech.VkApiClient.ApiModels
{
    public class Track
    {
        public string Title { get; set; }
        public string Artist { get; set; }
    }
}