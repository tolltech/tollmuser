﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Tolltech.VkApiClient.ApiModels;
using VkNet;
using VkNet.AudioBypassService.Extensions;
using VkNet.Enums.Filters;
using VkNet.Model;
using VkNet.Model.RequestParams;

namespace Tolltech.VkApiClient
{
    public class VkApiClient : IVkApiClient
    {
        private readonly string login;
        private readonly string password;
        private VkApi api = null;

        private VkApi vkApi
        {
            get
            {
                if (api != null)
                {
                    return api;
                }

                var serviceCollection = new ServiceCollection();
                serviceCollection.AddAudioBypass();
                var localApi = new VkApi(serviceCollection);

                localApi.Authorize(new ApiAuthParams
                {
                    ApplicationId = applicationId,
                    Login = login,
                    Password = password,
                    Settings = Settings.Audio
                });

                return api = localApi;
            }
        }

        private const int applicationId = 6805574;

        public VkApiClient(string login, string password)
        {
            this.login = login;
            this.password = password;
        }

        public async Task<Track[]> GetTracksAsync(int? pageSize = null)
        {
            List<Track> list = null;
            ulong totalCount;
            var currentOffset = 0L;
            do
            {
                var audios = await vkApi.Audio.GetAsync(new AudioGetParams {Offset = currentOffset, Count = pageSize})
                    .ConfigureAwait(false);
                totalCount = audios.TotalCount;
                currentOffset += audios.Count;

                list = list ?? new List<Track>((int) totalCount);

                list.AddRange(audios.Select(x => new Track
                {
                    Title = x.Title,
                    Artist = x.Artist
                }));
            } while ((ulong) list.Count < totalCount);

            return list.ToArray();
        }

        public Task<bool> AuthorizeAsync()
        {
            try
            {
                return Task.FromResult(vkApi != null);
            }
            catch (Exception e)
            {
                return Task.FromResult(false);
            }
        }
    }

    //workaround
    ////s = requests.post("https://vrit.me/data.php", data ={
    ////    "method": "audio.get",
    ////    "count": 1000000000,
    ////    "offset": 0,
    ////    "user_id":-52922518})

    //using (var webClient = new WebClient())
    //{
    //var body = new
    //{
    //    method = "audio.get",
    //    count = 1000000000,
    //    offset = 0,
    //    user_id = vkUserId
    //};
    //var response = await webClient.UploadDataTaskAsync("", body.ToFormData()).ConfigureAwait(false);
    //var s = Encoding.UTF8.GetString(response);
    //}
}