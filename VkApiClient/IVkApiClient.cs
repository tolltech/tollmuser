﻿using System.Threading.Tasks;
using JetBrains.Annotations;
using Tolltech.VkApiClient.ApiModels;

namespace Tolltech.VkApiClient
{
    public interface IVkApiClient
    {
        [ItemNotNull]
        [NotNull]
        Task<Track[]> GetTracksAsync(int? pageSize = null);

        [NotNull]
        Task<bool> AuthorizeAsync();
    }
}